import React, { useEffect } from 'react'
import constant from '../../Constant'
import Axios from 'axios'
import jwt_decode from "jwt-decode";


const Authentication = (props) => {

    const verifyUser = async () => {
        try {
            const response = await Axios.post(constant.verify_url + 'verify', {
                id: props.location.search.substring(5)
            })
            if (response.status === 200) {
                localStorage.setItem('token', response.data.token)
                const decoded = jwt_decode(response.data.token);
                props.history.push(`cache/${decoded.id}`)
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        localStorage.setItem('token', "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoibWF5dXJnYWlrd2FkNzQ3NEBnbWFpbC5jb20iLCJpZCI6IjYwN2U3OTc4NGM4MmUzMjM3ZWY3NjIwMSIsImlhdCI6MTYxOTQ5NTc4OSwiZXhwIjoxNjE5NTEwMTg5fQ.hS0fFRq62uePPf_ewkJicTwbRIR_n8ADlojNOlOGEvs")
        const decoded = jwt_decode("yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoibWF5dXJnYWlrd2FkNzQ3NEBnbWFpbC5jb20iLCJpZCI6IjYwN2U3OTc4NGM4MmUzMjM3ZWY3NjIwMSIsImlhdCI6MTYxOTQ5NTc4OSwiZXhwIjoxNjE5NTEwMTg5fQ.hS0fFRq62uePPf_ewkJicTwbRIR_n8ADlojNOlOGEvs");
        props.history.push(`cache/${decoded.id}`)
        /*if (!props.location.search.substring(5)) return window.location = "https://www.app.creatosaurus.io/"
        verifyUser()*/
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <div>

        </div>
    )
}

export default Authentication
