import React, { useState, useEffect } from 'react'
import '../PagesCss/Twitter.css'
import Axios from 'axios'
import constant from '../../Constant'

const Twitter = (props) => {
    const [image, setimage] = useState("")
    const [flag, setflag] = useState(0)
    const [fileType, setfileType] = useState(null)
    const [status, setstatus] = useState("")
    const [description, setdescription] = useState("")
    const [date, setdate] = useState("")
    const [file, setfile] = useState(null)
    const [currentDate, setcurrentDate] = useState(null)
    const [captions, setcaptions] = useState([])
    const [templates, settemplates] = useState([])

    useEffect(() => {
        let d = new Date()
        d.setMinutes(d.getMinutes() + 5);
        let yyyy = d.getFullYear()
        let MM = d.getMonth() + 1
        if (MM < 10) {
            MM = "0" + MM
        }
        let dd = d.getDate()
        if (dd < 10) {
            dd = "0" + dd
        }
        let hh = d.getHours()
        if (hh < 10) {
            hh = "0" + hh
        }
        let mm = d.getMinutes()
        if (mm < 10) {
            mm = "0" + mm
        }

        let todayDate = `${yyyy}-${MM}-${dd}T${hh}:${mm}`
        setdate(todayDate)
        setcurrentDate(todayDate)
        getCaptions()
        getTheTemplates()
    }, [])

    const getCaptions = async () => {
        try {
            let response = await Axios.get(`https://www.captionator.creatosaurus.io/captionator/saved/${props.location.state.userId}`)
            if (response.data === "") {
                setcaptions(["Nothing saved yet"])
            } else {
                setcaptions(response.data.saved)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getTheTemplates = async () => {
        try {
            let resposne = await Axios.get('https://muse.creatosaurus.io/muse/admin/templates')
            settemplates(resposne.data)
        } catch (error) {
            console.log(error)
        }
    }

    const SeeTheFileFormat = (e) => {
        setfileType(e.target.files[0].type)
        setfile(e.target.files[0])
        setimage(URL.createObjectURL(e.target.files[0]))
        setflag(1)
    }

    const selectTheVideoFile = (e) => {
        setfileType(e.target.files[0].type)
        setfile(e.target.files[0])
        setimage(URL.createObjectURL(e.target.files[0]))
        setflag(2)
    }

    const post = () => {
        if (fileType === "image/jpeg" || fileType === "image/png") {
            const url = constant.url + 'twitter/postmedia';
            const formData = new FormData();
            formData.append('images', file)
            formData.set('status', status);
            formData.set('description', description);
            formData.set('date', date);
            formData.set('id', props.location.state.id)

            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                },
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    console.log(percentCompleted)
                }
            }
            Axios.post(url, formData, config).then(data => {
                console.log(data)
                alert("Post Uploaded")
            }).catch(err => {
                console.log(err)
            })
        } else if (fileType === "video/mp4" || fileType === "video/mov") {
            const url = constant.url + 'twitter/video';
            const formData = new FormData();
            formData.append('images', file)
            formData.set('status', status);
            formData.set('date', date);
            formData.set('id', props.location.state.id)

            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            Axios.post(url, formData, config).then(data => {
                console.log(data)
            }).catch(err => {
                console.log(err)
            })
        }
    }

    const uploadStatus = () => {
        Axios.post(constant.url + 'twitter/tweet', {
            id: props.location.state.id,
            status: status,
            date: date
        }).then(data => {
            console.log(data)
        }).catch(err => {
            console.log(err)
        })
    }


    return (
        <div>
            <div style={{ display: 'flex' }}>
                <div>
                <input type="file" id="photoUpload" name="upload_media" style={{ display: 'none' }} accept="image/*" onChange={(e) => SeeTheFileFormat(e)} />
                <input type="file" id="videoUpload" name="upload_media" style={{ display: 'none' }} accept="video/*" onChange={(e) => selectTheVideoFile(e)} />
                <div style={{ backgroundColor: '#dcdcdc', padding: 10, borderRadius: 5, width: 100, marginTop: 10, marginLeft: 10 }} onClick={() => document.getElementById('photoUpload').click()}>Upload Photo</div>
                <div style={{ backgroundColor: '#dcdcdc', padding: 10, borderRadius: 5, width: 100, marginTop: 10, marginLeft: 10 }} onClick={() => document.getElementById('videoUpload').click()} >Upload Video</div>
                <div style={{ backgroundColor: '#dcdcdc', padding: 10, borderRadius: 5, width: 100, marginTop: 10, marginLeft: 10 }} onClick={() => setflag(3)} >add status</div>
                {
                    flag === 1 ?
                        <div className="modal">
                            <input type="text" className="status" placeholder="What's happening ?" onChange={(e) => setstatus(e.target.value)} />
                            <img src={image} style={{ height: 150, width: 150, objectFit: 'contain', borderRadius: 5 }} />
                            <div className="input-container">
                                <input type="text" className="description" placeholder="Add description" onChange={(e) => setdescription(e.target.value)} />
                                <input type="datetime-local" id="meeting-time" value={date} min={currentDate} onChange={(e) => setdate(e.target.value)} />
                                <button onClick={post}>Post</button>
                            </div>
                        </div>
                        : flag === 2 ?
                            <div className="modal">
                                <input type="text" className="status" placeholder="What's happening ?" onChange={(e) => setstatus(e.target.value)} />
                                <video controls={false} src={image} style={{ height: 200, width: 450, backgroundPosition: 'center', backgroundSize: 'cover', borderRadius: 10, marginBottom: 10 }} />
                                <div className="input-container">
                                    <input type="datetime-local" id="meeting-time" value={date} min={currentDate} onChange={(e) => setdate(e.target.value)} />
                                    <button onClick={post}>Post</button>
                                </div>
                            </div>
                            : flag === 3 ?
                                <div className="modal">
                                    <input type="text" className="status" placeholder="What's happening ?" onChange={(e) => setstatus(e.target.value)} />
                                    <div className="input-container">
                                        <input type="datetime-local" id="meeting-time" value={date} min={currentDate} onChange={(e) => setdate(e.target.value)} />
                                        <button onClick={() => uploadStatus()}>Post</button>
                                    </div>
                                </div>
                                : null
                }
                </div>
                <div className="caption" style={{margin:10}}>
                    <h2 style={{marginBottom:10}}>Captionater Data</h2>
                    {
                        captions.map(data => {
                            return <div className="caption-card">{data.substr(4)}</div>
                        })
                    }
                </div>
                <div className="images" style={{margin:10}}>
                    <h2 style={{marginBottom:10}}>Muse Images</h2>
                    {
                        templates.map(data => {
                          return  <img
                            className="template-image"
                            key={data._id}
                            src={data.image_url}
                            alt="loading" />
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default Twitter





